﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

/* SwitchView :
 *  0 : Accueil
 *  1 : Houses
 *  2 : Characters
 *  3 : Duels
 *  4 : CharacterEditor */

namespace ApplicationWPF
{
    class ViewModel : INotifyPropertyChanged
    {
        public static ViewModel instance = null;

        public int switchView;
        public int Param { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public int SwitchView
        {
            get { return switchView; }
            set
            {
                switchView = value;
                // Call OnPropertyChanged whenever the property is updated
                OnPropertyChanged("SwitchView");
            }
        }

        public static ViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ViewModel();
                }
                return instance;
            }
        }


        public ViewModel()
        {
            switchView = 0;
            instance = this;
        }

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
