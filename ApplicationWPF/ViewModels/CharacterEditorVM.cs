﻿using EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationWPF.ViewModels
{
    class CharacterEditorVM : ViewModelBase
    {
        private Character _character;

        public Character Character
        {
            get { return _character; }
            private set { _character = value; }
        }

        public CharacterEditorVM(Character c)
        {
            _character = c;
        }

        public string FirstName
        {
            get { return _character.FirstName; }
            set
            {
                if (value == _character.FirstName) return;
                _character.FirstName = value;
                base.OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get { return _character.LastName; }
            set
            {
                if (value == _character.LastName) return;
                _character.LastName = value;
                base.OnPropertyChanged("LastName");
            }
        }

        public int Health
        {
            get { return _character.Health; }
            set
            {
                if (value == _character.Health) return;
                _character.Health = value;
                base.OnPropertyChanged("Health");
            }
        }

        public int Bravoury
        {
            get { return _character.Bravoury; }
            set
            {
                if (value == _character.Bravoury) return;
                _character.Bravoury = value;
                base.OnPropertyChanged("Bravoury");
            }
        }

        public int Crazyness
        {
            get { return _character.Crazyness; }
            set
            {
                if (value == _character.Crazyness) return;
                _character.Crazyness = value;
                base.OnPropertyChanged("Crazyness");
            }
        }

        public int Strength
        {
            get { return _character.Strength; }
            set
            {
                if (value == _character.Strength) return;
                _character.Strength = value;
                base.OnPropertyChanged("Strength");
            }
        }
    }
}
