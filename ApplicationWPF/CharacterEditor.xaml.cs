﻿using ApplicationWPF.ViewModels;
using EntityLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApplicationWPF
{
    /// <summary>
    /// Logique d'interaction pour CharacterEditor.xaml
    /// </summary>
    public partial class CharacterEditor : UserControl
    {
        public CharacterEditor()
        {
            InitializeComponent();
            getCharacter(ViewModel.Instance.Param);
        }


        private void OnReturnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 2;
        }



        private async void OnSave(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Baudhuit");
            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { "firstname", FirstNameBox.Text },
                    { "lastname", LastNameBox.Text },
                    { "bravoury", BravouryBox.Text },
                    { "strength", StrengthBox.Text },
                    { "health", HealthBox.Text },
                    { "crazyness", CrazynessBox.Text }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync("http://localhost:4205/Character/Edit/" + ViewModel.Instance.Param, content);
                if (response.IsSuccessStatusCode)
                {
                    ViewModel.Instance.SwitchView = 2;
                }
                else
                {
                    // TODO afficher erreur
                }
            }
        }

        public async void getCharacter(int id)
        {
            Character c = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                
                HttpResponseMessage response = await client.GetAsync($"Character/Details/{id}");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    var item = (JObject) JsonConvert.DeserializeObject(temp);
                    System.Diagnostics.Debug.WriteLine(temp);

                    c = new Character() { ID = item.Value<int>("id"), FirstName = item.Value<String>("firstName"), LastName = item.Value<String>("lastName"), Bravoury = item.Value<int>("bravoury"), Crazyness = item.Value<int>("crazyness"), Strength = item.Value<int>("strength"), Health = item.Value<int>("health") };
                }
            }
            FirstNameBox.Text = c.FirstName;
            LastNameBox.Text = c.LastName;
            BravouryBox.Text = c.Bravoury.ToString();
            StrengthBox.Text = c.Strength.ToString();
            HealthBox.Text = c.Health.ToString();
            CrazynessBox.Text = c.Crazyness.ToString();

        }
    }
}
