﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApplicationWPF
{
    /// <summary>
    /// Logique d'interaction pour Accueil.xaml
    /// </summary>
    public partial class Accueil : UserControl
    {
        public Accueil()
        {
            InitializeComponent();
        }

        private void OnHouseClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 1;
        }

        private void OnCharacterClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 2;
        }

        private void OnDuelClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 3;
        }
    }
}
