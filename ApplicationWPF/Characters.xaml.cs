﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EntityLayer;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ApplicationWPF
{
    /// <summary>
    /// Logique d'interaction pour Characters.xaml
    /// </summary>
    public partial class Characters : UserControl
    {
        public Characters()
        {
            InitializeComponent();
            getCharacters();
        }

        private void OnReturnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 0;
        }

        private void OnClickItem(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cList.SelectedIndex != -1)
            {
                Character c = (Character)cList.SelectedItem;

                Console.WriteLine(c.FirstName + " " + c.ID);

                ViewModel.Instance.Param = c.ID;
                ViewModel.Instance.SwitchView = 4;
            }
        }


        public async void getCharacters()
        {
            List<Character> items = new List<Character>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("Character/Index");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    var obj = JsonConvert.DeserializeObject(temp);
                    System.Diagnostics.Debug.WriteLine(temp);
                    int i = 0;
                    foreach (var item in ((JArray)obj))
                    {
                        items.Add(new Character() { ID=item.Value<int>("ID"), FirstName = item.Value<String>("FirstName"), LastName = item.Value<String>("LastName"), Bravoury = item.Value<int>("Bravoury"), Crazyness = item.Value<int>("Crazyness"), Strength = item.Value<int>("Strength"), Health = item.Value<int>("Health") });
                        i++;
                    }
                }
            }
            cList.ItemsSource = items;
        }

    }
}
