﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EntityLayer;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ApplicationWPF
{
    /// <summary>
    /// Logique d'interaction pour UserControl2.xaml
    /// </summary>
    public partial class Houses : UserControl
    {
        public Houses()
        {
            InitializeComponent();
            getHouses();
        }

        private void OnReturnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 0;
        }

        public async void getHouses()
        {
            List<House> items = new List<House>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("House/Index");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    var obj = JsonConvert.DeserializeObject(temp);
                    System.Diagnostics.Debug.WriteLine(temp);
                    int i = 0;
                    foreach (var item in ((JArray)obj))
                    {
                        items.Add(new House() { Name = item.Value<String>("Name"), NumberOfUnits = item.Value<int>("NumberOfUnits") });
                        i++;
                    }
                }
            }
            hList.ItemsSource = items;
        }
    }
}
