﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EntityLayer;
using System.Net.Http;
using System.Net.Http.Headers;
using ApplicationWPF.Models;

namespace ApplicationWPF
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class Duels : UserControl
    {
        public Duels()
        {
            InitializeComponent();
            getDuels();
        }

        private void OnReturnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Instance.SwitchView = 0;
        }

        private void OnNewClick(object sender, RoutedEventArgs e)
        {
            getDuels();
        }

        public async void getDuels()
        {
            List<Duel> items = new List<Duel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:4205/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                // New Code:
                HttpResponseMessage response = await client.GetAsync("Duel/Index");
                if (response.IsSuccessStatusCode)
                {
                    string text = await response.Content.ReadAsStringAsync();
                    var arr = JArray.Parse(text);
                    foreach (var duelObj in arr)
                    {
                        var c1Obj = duelObj.SelectToken("id1");
                        var c2Obj = duelObj.SelectToken("id2");

                        items.Add(new Duel() { Name1 = (string)c1Obj.SelectToken("firstName") + " " + (string)c1Obj.SelectToken("lastName"), Name2 = (string)c2Obj.SelectToken("firstName") + " " + (string)c2Obj.SelectToken("lastName"), Cote1 = (float)duelObj.SelectToken("cote1"), Cote2 = (float)duelObj.SelectToken("cote2") });
                    }
                }
            }
            dList.ItemsSource = items;
        }
    }
}
