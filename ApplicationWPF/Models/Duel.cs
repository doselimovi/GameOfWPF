﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationWPF.Models
{
    class Duel
    {
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public float Cote1 { get; set; }
        public float Cote2 { get; set; }
    }
}
