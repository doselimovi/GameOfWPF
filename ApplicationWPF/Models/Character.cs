﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    public class Character
    {
        //Attributes
        public int ID { get; set; }
        public int Bravoury { get; set; } //Over 100
        public int Crazyness { get; set; } //Over 100
        public int Strength { get; set; } //Over 100
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Health { get; set; } //Over 100
        public Dictionary<int, RelationshipEnum> Relationships { get; set; }
        
        //Methods
        public void addRelation(int ID, RelationshipEnum type)
        {
            Relationships.Add(ID, type);
        }

    }

    public enum RelationshipEnum
    {
        FRIENDSHIP,
        LOVE,
        HATRED,
        RIVALRY
    }

    public enum CharacterTypeEnum
    {
        WARRIOR,
        WITCH,
        TACTICIAN,
        LEADER,
        LOSER
    }


}
